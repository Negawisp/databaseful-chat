<h2>Read Me</h2>
Class <i>edu.netcracker.negawisp.databasefulchat.database.DatabaseManager</i>
pretty much serves as a repository, functioning by methods ending with <i>DB</i>.<br>
Database only saves the registered users and gives information about them on boot up.<br>
<br>
Database described at <i>resources/dbconn.properties</i> should initially have
a sequenced table <i>users</i> in order for app to work properly.