package edu.netcracker.negawisp.databasefulchat.restcontrollers;

import edu.netcracker.negawisp.databasefulchat.dataasset.dto.NewChatDTO;
import edu.netcracker.negawisp.databasefulchat.dataasset.dto.NewMessageDTO;
import edu.netcracker.negawisp.databasefulchat.dataasset.dto.UserRegistrationDTO;
import edu.netcracker.negawisp.databasefulchat.service.ChatService;
import edu.netcracker.negawisp.databasefulchat.service.RegistrationService;
import edu.netcracker.negawisp.databasefulchat.service.SecretService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AlmightyController {
    private Logger logger = LoggerFactory.getLogger(AlmightyController.class);

    @Autowired ChatService chatService;
    @Autowired RegistrationService registrationService;

    @RequestMapping(method=RequestMethod.PUT)
    public ResponseEntity beginChat (
            @RequestBody NewChatDTO newChatDTO
            ) {
        logger.info("Beginning new chat with a topic {}.", newChatDTO.getChatName());
        return chatService.createChat(newChatDTO);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/register")
    public ResponseEntity registerUser (
            @RequestBody UserRegistrationDTO registrationDTO
            ) {
        logger.info("Registering new user {}", registrationDTO.getUserName());
        return registrationService.registerUser(registrationDTO);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/ch/{chatName}")
    public ResponseEntity postMessage (
            @RequestBody NewMessageDTO messageDTO,
            @PathVariable String chatName
            ) {
        logger.info("New message at a chat {}.", chatName);
        return chatService.addMessage(chatName, messageDTO);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/ch/{chatName}")
    public ResponseEntity getChatAsString (
            @PathVariable String chatName
            ) {
        logger.info("Getting chat {}.", chatName);
        return chatService.getChatAsString(chatName);
    }
}
