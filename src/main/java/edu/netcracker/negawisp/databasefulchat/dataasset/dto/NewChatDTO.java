package edu.netcracker.negawisp.databasefulchat.dataasset.dto;

public class NewChatDTO {
    private String chatName;
    private String adminUsername;

    public String getChatName() {
        return chatName;
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    public String getAdminUsername() {
        return adminUsername;
    }

    public void setAdminUsername(String adminUsername) {
        this.adminUsername = adminUsername;
    }

    public NewChatDTO() {
    }
}
