package edu.netcracker.negawisp.databasefulchat.dataasset.dto;

public class UserRegistrationDTO {
    private String userName;
    private String password;

    public UserRegistrationDTO() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
