package edu.netcracker.negawisp.databasefulchat.dataasset.pojo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Chat {
    private Logger logger = LoggerFactory.getLogger(Chat.class);

    private long   id;
    private User   admin;
    private User[] users;

    private List<Message> messages;

    public Chat(long id, User admin) {
        this.id = id;
        this.admin = admin;

        messages = new ArrayList<>();
    }

    public void addMessage(Message msg) {
        if (null == msg) {
            logger.warn("Chat {} received a NULL message.", id);
            return;
        }
        messages.add(msg);
    }

    public Logger getLogger() {
        return logger;
    }

    public long getId() {
        return id;
    }

    public User getAdmin() {
        return admin;
    }

    public User[] getUsers() {
        return users;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public String getChatAsString (String chatNameToShow) {
        StringBuilder sb = new StringBuilder(String.format("%s (%d)\n\n", chatNameToShow, id));

        for (Message msg : messages) {
            sb.append(msg.getSender().getUsername()).append(' ')
              .append(msg.getSendingTime()).append(' ')
              .append("No").append(msg.getId()).append("\n\n")
              .append(msg.getText()).append("\n\n\n");
        }
        return sb.toString();
    }
}
