package edu.netcracker.negawisp.databasefulchat.dataasset.pojo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;

public class Message {
    Logger logger = LoggerFactory.getLogger(Message.class);

    private long   id;
    private long   senderId;
    private long   chatId;
    private String text;
    private String sendingTime;

    private User   sender;
    private Chat   chat;


    public Message(long id, User sender, Chat chat, String text, String sendingTime) {
        logger.warn("Message..ctor doesn't check its initial arguments for validation.");
        this.id = id;
        this.chatId = chat.getId();
        this.senderId = sender.getId();
        this.text = text;
        this.sendingTime = sendingTime;

        this.chat = chat;
        this.sender = sender;
    }

    public long getId() {
        return id;
    }

    public long getSenderId() {
        return senderId;
    }

    public long getChatId() {
        return chatId;
    }

    public User getSender() {
        return sender;
    }

    public Chat getChat() {
        return chat;
    }

    public String getText() {
        return text;
    }

    public String getSendingTime() {
        return sendingTime;
    }
}
