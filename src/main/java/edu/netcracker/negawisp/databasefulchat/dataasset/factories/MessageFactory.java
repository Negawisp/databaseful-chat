package edu.netcracker.negawisp.databasefulchat.dataasset.factories;

import edu.netcracker.negawisp.databasefulchat.dataasset.pojo.Chat;
import edu.netcracker.negawisp.databasefulchat.dataasset.pojo.Message;
import edu.netcracker.negawisp.databasefulchat.dataasset.pojo.User;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MessageFactory {

    private long       _id;
    private User       _sender;
    private Chat       _chat;
    private String     _text;
    private DateFormat _sendingTime;

    public MessageFactory() {
        _id = 0;
        _text = "%messageText%";
    }


    public MessageFactory id (long id) {
        _id = id;
        return this;
    }

    public MessageFactory sender (User sender) {
        _sender = sender;
        return this;
    }

    public MessageFactory chat (Chat chat) {
        _chat = chat;
        return this;
    }

    public MessageFactory text (String text) {
        _text = text;
        return this;
    }

    public Message build () {
        return new Message(_id, _sender, _chat, _text, new SimpleDateFormat("dd/MM/YY EEE hh:mm:ss").format(new Date()));
    }
}
