package edu.netcracker.negawisp.databasefulchat.dataasset.pojo;

import javax.jws.soap.SOAPBinding;
import java.text.SimpleDateFormat;
import java.util.Date;

public class User {
    private long   id;
    private String username;
    private long   passwordHash;
    private String registrationDate;

    public User(long id, String username, long passwordHash) {
        this(   id,
                username,
                passwordHash,
                new SimpleDateFormat("dd-MM-YYYY").format(new Date()));
    }

    public User(long id, String username, long passwordHash, String registrationDate) {
        this.id = id;
        this.username = username;
        this.passwordHash = passwordHash;
        this.registrationDate = registrationDate;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }


    public long getPasswordHash() {
        return passwordHash;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }
}
