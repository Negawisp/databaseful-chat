package edu.netcracker.negawisp.databasefulchat.dataasset.dto;

public class NewMessageDTO {

    private String senderUsername;
    private String text;

    public NewMessageDTO() {
    }

    public String getSenderUsername() {
        return senderUsername;
    }

    public void setSenderUsername(String senderUsername) {
        this.senderUsername = senderUsername;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
