package edu.netcracker.negawisp.databasefulchat.database;

import org.springframework.stereotype.Service;

@Service
public class SQLBuilder {

    private String usersTableName = "users";

    public SQLBuilder () {}

    public String buildUserINSERT (String username, String passwordHash, String registrationDate) {
        StringBuilder sb = new StringBuilder("INSERT INTO ");
        sb.append(usersTableName)
                .append(" (username, password_hash, registration_date) VALUES ('")
                .append(username).append("', ")
                .append(passwordHash).append(", DATE '")
                .append(registrationDate).append("'")
                .append(')');
        return sb.toString();
    }
}
