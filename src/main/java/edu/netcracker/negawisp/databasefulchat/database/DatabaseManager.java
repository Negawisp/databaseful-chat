package edu.netcracker.negawisp.databasefulchat.database;

import edu.netcracker.negawisp.databasefulchat.dataasset.dto.NewChatDTO;
import edu.netcracker.negawisp.databasefulchat.dataasset.dto.NewMessageDTO;
import edu.netcracker.negawisp.databasefulchat.dataasset.dto.UserRegistrationDTO;
import edu.netcracker.negawisp.databasefulchat.dataasset.factories.MessageFactory;
import edu.netcracker.negawisp.databasefulchat.dataasset.pojo.Chat;
import edu.netcracker.negawisp.databasefulchat.dataasset.pojo.Message;
import edu.netcracker.negawisp.databasefulchat.dataasset.pojo.User;
import edu.netcracker.negawisp.databasefulchat.enumerators.DBResponce;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static edu.netcracker.negawisp.databasefulchat.service.SecretService.calculatePasswordHash;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Service
public class DatabaseManager {
    Logger logger = LoggerFactory.getLogger(DatabaseManager.class);

    private int messagesCount;
    private int usersCount;
    private int chatsCount;

    @Autowired
    private SQLBuilder sqlBuilder;
    private Connection connection;

    private Map<String, Chat> chats;
    private Map<String, User> sessionUsers;

    private static final String propertiesFile = "src\\main\\resources\\dbconn.properties";

    public DatabaseManager () {
        chats = new HashMap<>();
        sessionUsers = new HashMap<>();
        connect(propertiesFile);
        loadUsersFromDB();
    }

    private void putMessageToTable (Message message) {

    }

    public void connect (String propertiesFile)  {
        Properties p = new Properties();
        try {
            FileInputStream fis = new FileInputStream(propertiesFile);
            p.load(fis);
        } catch (FileNotFoundException e) {
            logger.error("There was no such file: {}", propertiesFile);
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("Couldn't load properties from file {}", propertiesFile);
            e.printStackTrace();
        }

        String dname = p.getProperty("Dname");
        String url = p.getProperty("URL");
        String user = p.getProperty("Uname");
        String password = p.getProperty("password");

        logger.info("Properties are read.");

        try {
            Class.forName(dname);
        } catch (ClassNotFoundException e) {
            logger.error("{} class is not found. Include it in your library path ", dname);
            e.printStackTrace();
            return;
        }

        logger.info("PostgreSQL JDBC Driver successfully connected");

        try {
            disconnect();
            connection = DriverManager
                    .getConnection(url, user, password);

        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
            return;
        }

        if (connection != null) {
            System.out.println("You successfully connected to database now");
        } else {
            System.out.println("Failed to make connection to database");
        }
    }

    public void disconnect () {
        try {
            if (null != connection) {
                connection.close();
            }
        } catch (SQLException e) {
            logger.error("Was unable to close disconnect from database.");
            e.printStackTrace();
        }
    }

    public DBResponce tryStoreMessage(String chatName, NewMessageDTO messageDTO) {
        Chat chat = chats.get(chatName);
        if (null == chat) {
            return DBResponce.NO_SUCH_CHAT;
        }

        User user = sessionUsers.get(messageDTO.getSenderUsername());
        if (null == user) {
            return DBResponce.NO_SUCH_USER;
        }

        MessageFactory mf = new MessageFactory();
        mf.id(++messagesCount)
          .text(messageDTO.getText())
          .sender(user)
          .chat(chat);
        Message message = mf.build();

        putMessageToTable(message);

        chat.addMessage(message);

        return DBResponce.OK;
    }

    public Chat getChatByName (String chatName) {
        return chats.get(chatName);
    }

    public DBResponce createChat (NewChatDTO newChatDTO) {
        logger.warn("createChat doesn't check a chatName for existence.");

        if (null == newChatDTO) {
            return DBResponce.NULL_ARGUMENT;
        }
        String adminUsername = newChatDTO.getAdminUsername();
        User admin = sessionUsers.get(adminUsername);
        if (null == admin) {
            return DBResponce.NO_SUCH_USER;
        }
        Chat chat = new Chat(++chatsCount, admin);

        chats.put(newChatDTO.getChatName(), chat);
        return DBResponce.OK;
    }

    public DBResponce registerUser(UserRegistrationDTO regDTO) {
        logger.warn("registerUser doesn't use Factory to create an instance of User.");

        if (null == regDTO) {
            logger.warn("Received null as regDTO");
            return DBResponce.NULL_ARGUMENT;
        }

        if (checkUserInDB(regDTO.getUserName())) {
            logger.info(DBResponce.USERNAME_TAKEN.getLogMessage(regDTO.getUserName()));
            return DBResponce.USERNAME_TAKEN;
        }

        User newUser = new User(++usersCount,
                regDTO.getUserName(),
                calculatePasswordHash(regDTO.getPassword()));

        sessionUsers.put(regDTO.getUserName(), newUser);
        storeUserToDB(newUser);
        return DBResponce.OK;
    }

    /**
     * Finds out whether user is registered to the database or not.
     * @param username User to check
     * @return TRUE, if user was found in database, or FALSE if they were not.
     */
    private boolean checkUserInDB (String username) {
        boolean retVal = false;
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM users WHERE username = '" + username + "'");
            if (rs.next()) { retVal = true; }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return retVal;
    }

    private void storeUserToDB (User newUser) {
        try {
            Statement stmt = connection.createStatement();
            String sqlInsert = sqlBuilder.buildUserINSERT(
                    newUser.getUsername(),
                    Long.toString(newUser.getPasswordHash()),
                    newUser.getRegistrationDate());
            logger.info("Executing command {}", sqlInsert);
            stmt.execute(sqlInsert);
            if (null != stmt) stmt.close();
        } catch (SQLException e) {
            logger.error("SQL exception");
            e.printStackTrace();
        }
    }

    private void loadUsersFromDB() {
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM users");
            while (rs.next()) {
                User user = new User(   rs.getInt(1),
                                        rs.getString(2),
                                        rs.getInt(3),
                                        rs.getDate(4).toString());
                sessionUsers.put(user.getUsername(), user);
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
