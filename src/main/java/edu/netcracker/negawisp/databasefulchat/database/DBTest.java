package edu.netcracker.negawisp.databasefulchat.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.sql.*;
import java.util.Properties;

public class DBTest {
    private static Logger logger = LoggerFactory.getLogger(DBTest.class);

    private static Connection connection;

    public static void connect(String propertiesFile) throws Exception {

        FileInputStream fis = new FileInputStream(propertiesFile);
        Properties p = new Properties();
        p.load(fis);

        String dname = p.getProperty("Dname");
        String url = p.getProperty("URL");
        String user = p.getProperty("Uname");
        String password = p.getProperty("password");

        logger.info("Properties are read.");


        try {
            Class.forName(dname);
        } catch (ClassNotFoundException e) {
            logger.error("{} class is not found. Include it in your library path ", dname);
            e.printStackTrace();
            return;
        }

        logger.info("PostgreSQL JDBC Driver successfully connected");

        try {
            closeConnection();
            connection = DriverManager
                    .getConnection(url, user, password);

        } catch (SQLException e) {
            System.out.println("Connection Failed");
            e.printStackTrace();
            return;
        }

        if (connection != null) {
            System.out.println("You successfully connected to database now");
        } else {
            System.out.println("Failed to make connection to database");
        }
    }

    public static void showTable (String tableName) throws SQLException {
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM " + tableName);
        while (rs.next()) {
            System.out.println(rs.getInt(1) + " " + rs.getString(2) + " "
                    + rs.getInt(3) + " " + rs.getDate(4));
        }
        rs.close();
        stmt.close();
    }

    public static void closeConnection () throws SQLException {
        if (null != connection) {
            connection.close();
        }
    }
}
