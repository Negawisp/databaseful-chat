package edu.netcracker.negawisp.databasefulchat.service;

import edu.netcracker.negawisp.databasefulchat.database.DatabaseManager;
import edu.netcracker.negawisp.databasefulchat.dataasset.dto.UserRegistrationDTO;
import edu.netcracker.negawisp.databasefulchat.enumerators.DBResponce;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {
    private Logger logger = LoggerFactory.getLogger(RegistrationService.class);

    @Autowired
    private DatabaseManager database;

    public ResponseEntity registerUser (UserRegistrationDTO regDTO) {
        if (DBResponce.USERNAME_TAKEN == database.registerUser(regDTO)) {
            return DBResponce.USERNAME_TAKEN.getHTTPResponse(regDTO.getUserName());
        }
        return ResponseEntity.ok().build();
    }
}
