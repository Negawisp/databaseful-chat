package edu.netcracker.negawisp.databasefulchat.service;

import edu.netcracker.negawisp.databasefulchat.database.DatabaseManager;
import edu.netcracker.negawisp.databasefulchat.dataasset.dto.NewChatDTO;
import edu.netcracker.negawisp.databasefulchat.dataasset.dto.NewMessageDTO;
import edu.netcracker.negawisp.databasefulchat.dataasset.pojo.Chat;
import edu.netcracker.negawisp.databasefulchat.enumerators.DBResponce;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ChatService {
    Logger logger = LoggerFactory.getLogger(ChatService.class);

    @Autowired
    private DatabaseManager database;

    public ResponseEntity addMessage (String chatName, NewMessageDTO messageDTO) {
        database.tryStoreMessage(chatName, messageDTO);
        return ResponseEntity.ok().build();
    }

    public ResponseEntity getChatAsString (String chatName) {
        Chat chat = database.getChatByName(chatName);
        if (null == chat) {
            logger.info("Tried to get a non existing chat {}", chatName);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(String.format("Chat %s does not exist.", chatName));
        }
        return ResponseEntity.ok(chat.getChatAsString(chatName));
    }

    public ResponseEntity createChat (NewChatDTO newChatDTO) {
        if (null != database.getChatByName(newChatDTO.getChatName())){
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(String.format("Chat %s already exists.", newChatDTO.getChatName()));
        }
        if (DBResponce.USERNAME_TAKEN == database.createChat(newChatDTO)) {
            return DBResponce.USERNAME_TAKEN.getHTTPResponse(newChatDTO.getAdminUsername());
        }
        return getChatAsString(newChatDTO.getChatName());
    }
}
