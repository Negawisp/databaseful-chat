package edu.netcracker.negawisp.databasefulchat.service;

import org.springframework.stereotype.Service;


@Service
public class SecretService {
    public static long calculatePasswordHash (String password) {
        long hash = 0;
        char[] chars = password.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            hash += chars[i];
            hash = hash >> i;
        }
        return hash;
    }
}
