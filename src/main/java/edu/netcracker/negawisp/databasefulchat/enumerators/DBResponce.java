package edu.netcracker.negawisp.databasefulchat.enumerators;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public enum DBResponce {
    OK              ("OK"),
    NULL_ARGUMENT   ("Argument \"%s\" was null"),
    NO_SUCH_USER    ("Chat \"%s\" doesn't exist"),
    NO_SUCH_CHAT    ("User \"%s\" is not registered"),
    USERNAME_TAKEN  ("Username \"%s\" has already been taken"),
    UNHANDLED_EXCEPTION ("Something went wrong. Please, contact us via BlindEyed@yandex.ru.");



    private String message;

    DBResponce (String message) {
        this.message = message;
    }

    /**
     * Returns an HTTP response entity according to data base's response.
     * @param errorArgument A chat that didn't exist, a user that was not registered, e.t.c.
     * @return
     */
    public ResponseEntity<String> getHTTPResponse (String errorArgument) {
        if (OK.message.equals(message)) {
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(String.format(message, errorArgument));
    }

    /**
     *
     * @param errorArgument A variable that was null, a chat that didn't exist, e.t.c.
     * @return
     */
    public String getLogMessage (String errorArgument) {
        return String.format(message, errorArgument);
    }
}
