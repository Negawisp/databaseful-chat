package edu.netcracker.negawisp.databasefulchat;

import edu.netcracker.negawisp.databasefulchat.database.DBTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    public static void main (String[] args) throws Exception {
        /*
        DBTest.connect("src\\main\\resources\\dbconn.properties");
        DBTest.showTable("users");
        DBTest.closeConnection();
        */

        SpringApplication.run(Application.class, args);
    }
}
